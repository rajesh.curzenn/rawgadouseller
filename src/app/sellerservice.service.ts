import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SellerserviceService {

  // hosturl = "http://localhost:4201/"
  // baseapiurl = "http://localhost:9001/admin-panel/"
  // baseapiurl2 = "http://localhost:9001/"

  hosturl = "https://vendeur.rawgadou.com/"
  baseapiurl = "https://mainserver.rawgadou.com/admin-panel/"
  baseapiurl2 = "https://mainserver.rawgadou.com/"
  main_id: any; token: any; email: any; sessioin: any
  constructor() {
    this.main_id = localStorage.getItem('main_sellerid')
    this.token = localStorage.getItem('token')
    this.email = localStorage.getItem('selleremail')
    this.sessioin = localStorage.getItem('loginsession')

    if (this.sessioin >= '24') {
      setTimeout(() => {
        localStorage.clear()
        alert("Your session timeout")
        window.location.reload()
      }, 1000 * 60 * 60 * 24);
    }

  }
}
