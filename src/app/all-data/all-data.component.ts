import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables)

@Component({
  selector: 'app-all-data',
  templateUrl: './all-data.component.html',
  styleUrls: ['./all-data.component.css']
})
export class AllDataComponent implements OnInit {
  mainid:any
  baseurl:any
  all_data:any
  constructor(private seller:SellerserviceService, private http:HttpClient,
    private router:Router, private activateroute:ActivatedRoute) { 
    this.mainid = localStorage.getItem('main_sellerid') ,"thislocal"
    this.baseurl = seller.baseapiurl
  }

  ngOnInit(): void {
   
    console.log(localStorage.getItem('main_sellerid') ,"thislocal")
    console.log(this.mainid)

    this.http.get(this.baseurl+"renderhomepageSeller/"+this.mainid).subscribe(res=>{
      
      this.all_data = res

      console.log(this.all_data)
      if(this.all_data.data.totoalnumberoforders > 0){
      new Chart('piechart', {
        type: 'doughnut',
        data: {
          labels: [
            'Total',
            'Terminé',
            'Traitement',
            'Annulés',
            'Remboursé',
          ],
          datasets: [{
            label: '',
            data:this.all_data.data.orderspie,
  
            backgroundColor: [
              '#2171ae',
              '#28a745',
              '#ffc107',
              'rgb(255, 69, 96)',
              '#17a2b8'
            ],
            hoverOffset: 4
          }]
        },
        options: {
         
          plugins: {
            legend: {
              position: 'left'
            }
  
          }
  
        }
      });
      }

      if(this.all_data.data.totalearnings > 0){
      new Chart('bar', {
        type: 'bar',
        data: {
          labels: this.all_data.data.numberofdaysarray,
          datasets: [{
            label: 'ventes',
            data:this.all_data.data.totalearningsarray,
  
            backgroundColor: [
              'orange',
              
            ],
          }]
        },
        options: {
          scales: {
            x: {
              grid: {
                display: false,
              },
            },
            y: {
              grid: {
                display: false,
              },
            },
          },
        },
      });
    }
    })
    
   
  }

}
