import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, AbstractControl, FormControl, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CategoriesComponent } from '../categories/categories.component';


@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  allpics: any;apiResponse:any;base_url:string='';
  userForm!: FormGroup;
  phone: any;
  public products = [
    {
      id: 1,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 2,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 3,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 4,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 5,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 6,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 7,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 8,
      imgUrl: "",
      imgBase64Data: ""
    },
    {
      id: 9,
      imgUrl: "",
      imgBase64Data: ""
    }
  ];
  url: any
  format: any
  baseurl: any;
  base_url_front:any;
  token: any
  user_type: any
  productarray: any
  product: any
  form!: FormGroup
  addSection5!: FormGroup
  main_id: any
  formBuilder: any;
  myFiles: any;
  myFilesVideo: any;

  myFilesNew0:any; myFilesNew1:any; myFilesNew2:any; myFilesNew3:any; myFilesNew4:any;
  myFilesNew5:any; myFilesNew6:any; myFilesNew7:any; myFilesNew8:any; myFilesNew9:any;
  myFilesNew10:any; myFilesNew11:any; myFilesNew12:any; myFilesNew13:any; myFilesNew14:any;
  myFilesNew15:any; myFilesNew16:any; myFilesNew17:any; myFilesNew18:any; myFilesNew19:any;

  firstMainVariationVal:string='';secondMainVariationVal:string='';
  firstSubVariationArray :any;secondSubVariationArray:any;
  
  priceVariation0:any;priceVariation1:any;priceVariation2:any;priceVariation3:any;

  mydispatch_info: any
  mysize: any
  width: any; length: any; height: any; weight: any; getshippingMethodsByweight: any
  recordsship: any; weightval: any; allCategories: any; parentCategories: any;subparentCategories: any;
  subparentCategories2:any;
  subparentCategories3:any;
  subparentCategories4:any;
  subparentCategories5:any;
  simplecontent:any; variablecontent:any;myvariation:any;myProperty:any;abc:any
  mytestarray:any;
  productForm!: FormGroup;
  variationNameOne:any;subVariationNameOne:any;
  variationNameTwo:any;subVariationNameTwo:any;
  formData:any;
  old_attr_price:any;old_attr_stock:any;old_attr_SKU:any;
  category_id:any = {}; catApiResponse:any;

  displayStyle3 = "none"; displayStyle4 = "none"; displayStyle5 = "none"; 
  displayStyle6 = "none";
  
  displayStyle = "none";
  loading:any ; regdis:any
  constructor(
    private fb: FormBuilder, private seller: SellerserviceService,
    private http: HttpClient, private activeRoute: ActivatedRoute, private router: Router,
    public dialog: MatDialog,) {
      this.myFilesNew0 = [];this.myFilesNew1 = [];this.myFilesNew2 = [];this.myFilesNew3 = [];this.myFilesNew4 = [];this.myFilesNew5 = [];this.myFilesNew6 = [];this.myFilesNew7 = [];this.myFilesNew8 = [];this.myFilesNew9 = [];

      this.priceVariation0 = [];this.priceVariation1 = [];
      this.priceVariation2 = [];this.priceVariation3 = [];


      this.firstSubVariationArray = [];this.secondSubVariationArray = [];
      this.myFilesNew10 = [];this.myFilesNew11 = [];this.myFilesNew12 = [];this.myFilesNew13 = [];this.myFilesNew14 = [];this.myFilesNew15 = [];this.myFilesNew16 = [];this.myFilesNew17 = [];this.myFilesNew18 = [];this.myFilesNew19 = [];
    this.main_id = localStorage.getItem('main_sellerid')
      this.base_url_front = seller.hosturl;
    this.baseurl = seller.baseapiurl2
    this.base_url = seller.baseapiurl2;
    this.productarray = []
    this.product = []
    this.allpics = []
    this.myFiles = []
    this.subparentCategories  =[];
    this.subparentCategories2  =[];
    this.subparentCategories3  =[];
    this.subparentCategories4  =[];
    this.subparentCategories5  =[];
    
    this.allCategories = []
    this.mydispatch_info = {}
    this.mysize = {}
    this.simplecontent = false
    this.variablecontent = true
    this.myvariation = 'color'
    this.myProperty = ''
    this.getshippingMethodsByweight = []
    this.parentCategories = []
    this.mytestarray = []
    // this.userForm = this.fb.group({
     
    //   phones: this.fb.array([{
    //     name:this.fb.control(null)
    // }]),
    //   phones2: this.fb.array([this.fb.control(null)
    //   ]),
      
    // })
    this.addSection5 = new FormGroup({
      parent_sku: new FormControl('SKU'),
      condition: new FormControl(),
      preorder: new FormControl(true),
      shippingtime: new FormControl("in days"),
    })
    this.form = this.fb.group({
      sku: new FormControl('', [Validators.required]),
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),
      stock: new FormControl('', [Validators.required]),
      catagory: new FormControl('', [Validators.required]),
      dangerous_goods: new FormControl('', [Validators.required]),
      video: new FormControl('', []),
      dispatch_info: new FormControl(),
      provider_id: new FormControl(this.main_id, [Validators.required]),

      weight: new FormControl('', []),
      height: new FormControl('', []),
      length: new FormControl('', []),
      width: new FormControl('', []),

      //seller_will_bear_shipping: new FormControl('', []),
      enable_variation: new FormControl('', [Validators.required]),
      is_auction: new FormControl('', [Validators.required]),
      condition: new FormControl('new', []),
      sub_cat_1: new FormControl('', []),
      sub_cat_2: new FormControl('', []),
      sub_cat_3: new FormControl('', []),
      sub_cat_4: new FormControl('', []),
      sub_cat_5: new FormControl('', []),
      sub_cat_6: new FormControl('', []),
      auction_start_date: new FormControl('', []),
      auction_end_date: new FormControl('', []),
      auction_price: new FormControl('', []),
      quantities: this.fb.array([]),
    });


    this.loading = false
    this.regdis = false

    

  }


   

  ngOnInit(): void {
    this.getCategories();

    this.myFilesVideo = [];
  }
  quantities() : FormArray {
    return this.form.get("quantities") as FormArray
  }
  addQuantity() {
    this.quantities().push(this.newQuantity());
  }
  newQuantity(): FormGroup {
    return this.fb.group({

      attribute: '',
      value: '',
    })
  }
  removeQuantity(i:number) {
    this.quantities().removeAt(i);
  }
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: FormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }

  auctionFunction(event: any)
  {
    console.log("event.target.value ",event.target.value);
    if(event.target.value == "true")
    {
      console.log("ifffffffffffffffffffffff");
      this.displayStyle = "block";
    }else{
      this.displayStyle = "none";
    }
  }
  onFileUpdate(event: any, index: any) {
     
    const files = event.target.files;

    if (files.length === 0) return;

    const reader = new FileReader();

    reader.readAsDataURL(files[0]);
    reader.onload = _event => {

      this.products[index].imgBase64Data = reader.result as string;
      // this.myFiles.push(event.target.files);
      // console.log(this.myFiles)
      this.allpics.push(event.target.files);
      
      
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }

      // console.log(this.allpics)


    };
  }
  onSelectFile(event: any) {
    this.myFilesVideo = [];
    const file = event.target.files && event.target.files[0];
    
    
    if (event.target.files.length > 0)
      {
        //const video_file = event.target.files[0];
        //this.video = video_file;
        this.myFilesVideo.push(event.target.files[0]);
        //console.log(file);
         
      }


    if (file) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      if (file.type.indexOf('image') > -1) {
        this.format = 'image';
      } else if (file.type.indexOf('video') > -1) {
        this.format = 'video';
      }
      reader.onload = (event) => {
        this.url = (<FileReader>event.target).result;
      }
    }
  }

 

  
  sendWeight(values: any) {
    //console.log(this.weightval)

    //console.log(values.currentTarget.checked);
    if (values.currentTarget.checked) {
      this.http.post(this.baseurl + "api/shipping/getshippingMethodsByweight", { "weight": this.weightval }).subscribe(res => {
        this.getshippingMethodsByweight = res
        this.recordsship = this.getshippingMethodsByweight.data
        //console.log(this.recordsship)

      })
    }
    else {
      this.recordsship = []
    }

  }
  weightHere(weight: any) {
    this.weightval = parseInt(weight.target.value)
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CategoriesComponent, {
      height: '600px',
      width: '700px',
      position: {
        top: '12vh',
        left: '30vw'
      },
    });
  }
  getCategories() {
    this.http.get(this.baseurl + "api/product/getallcatagoriesflat").subscribe(res => {
      this.allCategories = res
      const categories = this.allCategories.data.filter((x: any) => {

        return x.mastercatagory == null
      })
      if (categories) {
        this.parentCategories = categories
        //console.log(this.parentCategories, "test")
      }
    })
  }
  onSelect(id:any){
    this.category_id["0"] = id.target.value;
    console.log(this.category_id," this.category_id");
    const categories = this.allCategories.data.filter((x: any) => {
      return x.mastercatagory == id.target.value
    })
    if (categories) {
      this.subparentCategories = categories
      //console.log(categories, "test2")
    }
  }
  onSubSelect1(id:any)
  {
    this.category_id["1"] = id.target.value;
    console.log(this.category_id," this.category_id");
    this.http.get(this.baseurl + "api/product/getOnlyChildCategory/"+id.target.value).subscribe(res => {
      console.log(res);
      this.catApiResponse = res;
      if(this.catApiResponse.status == true)
      {
        this.displayStyle3 = "block";
        this.subparentCategories2  = this.catApiResponse.data;
      }
    });
  }
  onSubSelect2(id:any)
  {
    this.category_id["2"] = id.target.value;
    console.log(this.category_id," this.category_id");
    this.http.get(this.baseurl + "api/product/getOnlyChildCategory/"+id.target.value).subscribe(res => {
      console.log(res);
      this.catApiResponse = res;
      if(this.catApiResponse.status == true)
      {
        this.displayStyle4 = "block";
        this.subparentCategories3  = this.catApiResponse.data;
      }
    });
  }
  onSubSelect3(id:any)
  {
    this.category_id["3"] = id.target.value;
    console.log(this.category_id," this.category_id");
    this.http.get(this.baseurl + "api/product/getOnlyChildCategory/"+id.target.value).subscribe(res => {
      console.log(res);
      this.catApiResponse = res;
      if(this.catApiResponse.status == true)
      {
        this.displayStyle5 = "block";
        this.subparentCategories4  = this.catApiResponse.data;
      }
    });
  }
  onSubSelect4(id:any)
  {
    this.category_id["4"] = id.target.value;
    console.log(this.category_id," this.category_id");
    this.http.get(this.baseurl + "api/product/getOnlyChildCategory/"+id.target.value).subscribe(res => {
      console.log(res);
      this.catApiResponse = res;
      if(this.catApiResponse.status == true)
      {
        this.displayStyle6 = "block";
        this.subparentCategories5  = this.catApiResponse.data;
      }
    });
  }
  onSubSelect5(id:any)
  {
    this.category_id["5"] = id.target.value;
    console.log(this.category_id," this.category_id");
  }
  simpleView(){
    this.simplecontent = false
    this.variablecontent = true
  }
  variableView(){
    this.simplecontent = true
    this.variablecontent = false
  }

  getVariations(variation:any){
    //console.log(variation.target.value)
    this.myvariation  = variation.target.value
  }
  onKeydownEvent(eve:any){
    //console.log(eve)
  }

  submit(){
    if (this.form.valid)
    {
       this.loading = true
       this.regdis = true

      let dispatch_info = {"weight":this.form.value.weight,"size":{"width":this.form.value.width,"Length":this.form.value.length,"Height":this.form.value.height},
          "shipping_details":{
            "city":""
          }
        }



      this.formData = new FormData();  
        this.formData.append('title', this.form.value.title);
        this.formData.append('provider_id', this.form.value.provider_id);
        
        this.formData.append('description', this.form.value.description);
        this.formData.append('catagory', this.form.value.catagory);
        this.formData.append('category_id', JSON.stringify(this.category_id));
        this.formData.append('sub_cat_1', this.form.value.sub_cat_1);
        this.formData.append('dangerous_goods', this.form.value.dangerous_goods);
        this.formData.append('dispatch_info', JSON.stringify(dispatch_info));
        this.formData.append('seller_will_bear_shipping', false);
        //this.formData.append('seller_will_bear_shipping', this.form.value.seller_will_bear_shipping);
        this.formData.append('condition', this.form.value.condition);
        this.formData.append('sku', this.form.value.sku);
        this.formData.append('enable_variation', this.form.value.enable_variation);
        this.formData.append('price', this.form.value.price);
        this.formData.append('stock', this.form.value.stock);
        this.formData.append('is_auction', this.form.value.is_auction);
        this.formData.append('auction_start_date', this.form.value.auction_start_date);
        this.formData.append('auction_end_date', this.form.value.auction_end_date);
        this.formData.append('auction_price', this.form.value.auction_price);

        console.log( " this.myVideos   " ,this.myFilesVideo.length);
        console.log( " this.myVideos   " ,this.myFilesVideo);
        // if(this.myFilesVideo)
        // {
        //   console.log( " herrrr   ");
        //   for (var i = 0; i < this.myFilesVideo.length; i++)
        //   { 
        //     console.log( " herrrr  for ");
        //     this.formData.append("video", this.myFilesVideo[i]);
        //   }
        // }
        console.log(this.myFiles , " this.myFiles");
        for (var i = 0; i < this.myFiles.length; i++)
        { 
          this.formData.append("photo", this.myFiles[i]);
        }
        if(this.myFilesVideo)
        {
          console.log( " herrrr   ");
          for (var i = 0; i < this.myFilesVideo.length; i++)
          { 
            this.formData.append("video", this.myFilesVideo[i]);
          }
        }
        var spec_attributes = this.form.value.quantities;
        this.formData.append('spec_attributes', JSON.stringify(spec_attributes));
        //var tagg = this.form.value.quantities;
        // console.log(tagg);
        // var spec_attributes = [];
        // if(tagg)
        // {
        //   for(let x=0; x<tagg.length; x++)
        //   {
        //     spec_attributes.push(tagg[x]);
        //   }
        // }
        // console.log(spec_attributes);
         this.http.post(this.baseurl+"api/product/createproduct", this.formData).subscribe(res => {
          
            console.log(res);

            this.apiResponse = res;
            if(this.apiResponse.status == true)
            {
              if(this.form.value.enable_variation == 'true')
              {
                let product_id = this.apiResponse.product_id;
                setTimeout(() => {
                  //window.location.reload();
                  //http://localhost:4200/dashboard/(seller:add_Variation)?id=6450b203b7bcb957a92a8a72
                  window.location.href = this.base_url_front+"dashboard/(seller:add_Variation)/"+"?id="+product_id;
                }, 2000); 
              }else{
                
                setTimeout(() => {
                  window.location.reload();
                }, 2000); 
              }
              

            }
            // alert("Product Saved succesfully")
            // window.location.href='/dashboard/(seller:myproducts)'
            // this.router.navigate(['myproducts'])
            //console.log(res)
          })
      //console.log(this.formData);
    }else{
      
      // console.log( " this.myVideos   " ,this.myFilesVideo.length);
      //   console.log( " this.myVideos   " ,this.myFilesVideo);
      //   if(this.myFilesVideo)
      //   {
      //     this.formData = new FormData();  
      //     console.log( " herrrr   ");
          
      //     for (var i = 0; i < this.myFilesVideo.length; i++)
      //     { 
      //       this.formData.append("video", this.myFilesVideo[i]);
      //     }

      //     this.http.post(this.baseurl+"api/product/createproductTest", this.formData).subscribe(res => {
      //       console.log(res)
      //       // alert("Product Saved succesfully")
      //       // window.location.href='/dashboard/(seller:myproducts)'
      //       // this.router.navigate(['myproducts'])
      //       //console.log(res)
      //     })
      //   }

      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}









