import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-shop-profile',
  templateUrl: './shop-profile.component.html',
  styleUrls: ['./shop-profile.component.css']
})
export class ShopProfileComponent implements OnInit {
  main_id:any
  registerform !:FormGroup; baseurl:any;getstatus:any;main_email:any
  constructor(private seller:SellerserviceService, private http:HttpClient,
    private router:Router, private activateroute:ActivatedRoute) {
      this.baseurl = seller.baseapiurl2
      this.main_id = localStorage.getItem('main_sellerid')
      this.main_email = localStorage.getItem('selleremail')
      this.registerform = new FormGroup({ 
        shopname: new FormControl(''),
       
        shopphoto:new FormControl(''),
        shopdesc:new FormControl(''),
        pickupaddress: new FormControl(''),
       
       
       })
     }

  ngOnInit(): void {
  }
  register(){
    this.registerform.value.id = this.main_id
    this.registerform.value.email = this.main_email
    console.log(this.registerform.value)
    this.http.post(this.baseurl+"api/seller/createseller" , this.registerform.value ).subscribe(res=>{
      
      this.getstatus =res
      if(this.getstatus.status == true){
        alert(this.getstatus.message)
        window.location.reload()
     }
   })
  }

}
