import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables)

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  baseurl:any;main_id:any;token:any;hostUrl:any
  constructor(private seller:SellerserviceService, private http:HttpClient,
    private router:Router, private activateroute:ActivatedRoute) {
      this.baseurl = seller.baseapiurl2
      this.main_id = localStorage.getItem('main_sellerid')
      this.token = localStorage.getItem('token')
      this.hostUrl = seller.hosturl
     }

  ngOnInit(): void {

    console.log( this.main_id, this.token )
    
    if(this.main_id == null || this.token == null){
      window.location.href=this.hostUrl+"login"
    }

    

   
  }

}
