import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-myincome',
  templateUrl: './myincome.component.html',
  styleUrls: ['./myincome.component.css']
})
export class MyincomeComponent implements OnInit {
  baseurl:any;main_id:any;incomedata:any;searchText:any
  apires:any
  sellerincomePostMethod:any; sellerincomePostMethodRes:any
  constructor(private seller:SellerserviceService, private http:HttpClient,
    private router:Router, private activateroute:ActivatedRoute) {
      this.baseurl = seller.baseapiurl2
      this.main_id = localStorage.getItem('main_sellerid')
      this.sellerincomePostMethod = this.baseurl +"api/seller/sellerincomePostMethod"

     }

  ngOnInit(): void {
    this.getInocme()
  }
  getInocme(){
    this.http.get(this.baseurl +"api/seller/sellerincome/"+this.main_id).subscribe(res=>{
      this.apires = res
      if(this.apires.status){
        this.incomedata = this.apires
      }
      

    })
  }

  filterTable(){
    const params={
      "id":this.main_id,
      "start_date":"2023-08-17",
      "end_date":"2023-08-18"
    }

    this.http.post(this.sellerincomePostMethod, params).subscribe(res=>{
      this.sellerincomePostMethodRes = res
      if(this.sellerincomePostMethodRes.status){
        this.incomedata = this.sellerincomePostMethodRes
       // console.log(this.sellerincomePostMethodRes )
      }
    })


  }
}
