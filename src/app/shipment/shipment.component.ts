import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr'
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-shipment',
  templateUrl: './shipment.component.html',
  styleUrls: ['./shipment.component.css']
})
export class ShipmentComponent implements OnInit {

  main_id: any
  baseurl: any
  ordersarray: any
  orders: any;
  myoptions: any
  searchText: any
  showerror: any
  pendinglist: any; completedlist: any
  getstatus: any
  start_date: any; end_date: any
  filterform!:FormGroup
  constructor(private seller: SellerserviceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute, public toastr: ToastrService) {
    this.main_id = localStorage.getItem('main_sellerid')
    this.baseurl = seller.baseapiurl2
    this.ordersarray = []
    this.orders = []
    this.pendinglist = []
    this.completedlist = []
    this.myoptions = {
      start_date: "",
      end_date: "",
      order_status: "",
      id: this.main_id
    }
    this.filterform = new FormGroup({
      start_date: new FormControl(''),
      end_date: new FormControl(''),

    })


  }

  ngOnInit(): void {
    this.http.post(this.baseurl + "api/order/getallordersbysellerid", this.myoptions).subscribe(res => {
      this.ordersarray = res
      this.orders = this.ordersarray.data
      console.log(this.orders, "this.orders")
      const Pending = this.orders.filter((x: any) => {
        return x.status == 0
      })
      if (Pending) {
        this.pendinglist = Pending
      }
      const Completd = this.orders.filter((x: any) => {
        return x.status == 1
      })
      if (Completd) {
        this.completedlist = Completd
      }

    })
  }
  pickup(eve: any, order_id: any) {

    console.log(eve)
    console.log(order_id)
    const readytoship = {

      order_id: order_id,
      provider_id: this.main_id,
      product_id: eve.products.product._id

    }

    console.log(readytoship)

    var ConfPick = confirm("voulez-vous ajouter cet article ?")
    if (ConfPick == true) {
      console.log(ConfPick)
      //  this.http.get(this.baseurl + "api/order/markOderasReadytopickup/" + id).subscribe(res => {
      this.http.post(this.baseurl + "api/order/markOderasReadytopickup", readytoship).subscribe(res => {
        this.getstatus = res
        console.log(res)
        if (this.getstatus.status == true) {
          this.toastr.success(this.getstatus.message, "Success", {
            timeOut: 3000,
          })
          // alert(this.getstatus.message)
          window.location.reload()
        } else {
          this.toastr.error(this.getstatus.message, "Error", {
            timeOut: 3000,
          })
          //alert(this.getstatus.message)
          // window.location.reload()
        }
      })
    }

  }
  filterDate() {

    // this.myoptions.start_date = this.start_date.value
    // this.myoptions.end_date = this.end_date.value

    const params = {
      id: this.main_id,
      start_date: new Date(this.filterform.value.start_date).toLocaleDateString(),
      end_date: new Date(this.filterform.value.end_date).toLocaleDateString(),
    }
    console.log(params)

   
    this.http.post(this.baseurl + "api/order/getallordersbysellerid", params  ).subscribe(res => {

      this.ordersarray = res
      this.orders = this.ordersarray.data
      console.log(this.orders)
      const Pending = this.orders.filter((x: any) => {
        return x.status == 0
      })
      if (Pending) {
        this.pendinglist = Pending
      }
      const Completd = this.orders.filter((x: any) => {
        return x.status == 1
      })
      if (Completd) {
        this.completedlist = Completd
      }

    })

  }



}
