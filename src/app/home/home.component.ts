import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  main_id:any;
  token:any;email:any
  withoutdetails = true
  withdetails = true
  baseurl:any; 
  getSubscriptionUrl:any; 
  getSubscriptiondata:any
  constructor(private seller: SellerserviceService, private http: HttpClient,) { 
    this.baseurl = seller.baseapiurl2
    this.main_id = localStorage.getItem('main_sellerid')
    this.token = localStorage.getItem('token')
    this.email = localStorage.getItem('selleremail')

    this.getSubscriptionUrl = this.baseurl + 'api/subs/getAllSubscriptions'
  }

  ngOnInit(): void {
    this.http.get(this.getSubscriptionUrl).subscribe(res => {
      this.getSubscriptiondata = res
    
    })

    if(this.main_id != null || this.token != null || this.email!= null ){
      this.withdetails = false;
    }else{
      this.withoutdetails = false
    }
  }

}
