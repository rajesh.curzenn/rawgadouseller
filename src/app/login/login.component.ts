import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import {ToastrService} from 'ngx-toastr'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginform!: FormGroup
  baseurl: any
  myoption: any
  getstatus:any
  visible: boolean = true;
  changetype: boolean = false;
  submitted = false;
 
  constructor(private seller: SellerserviceService, private http: HttpClient,
    private router: Router,public toastr: ToastrService) {
    this.baseurl = seller.baseapiurl2
    this.loginform = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      password: new FormControl('',  Validators.required)
    })
    // this.myoption ={
      //for this id 63f5c1ec5bf0bda586960971
    //   email:"stilldoingthis435@neverfwanted.com",
    //   password:"123456789",
    //   fcm_token:""
    //   }
  }

  ngOnInit(): void {
    localStorage.clear()
  }
  viewpass() {
    this.visible = !this.visible;
    this.changetype = !this.changetype;
  }
  
  get email() {
    return this.loginform.get('email')!;
  }
 

 
  login() {
    this.submitted = true
    this.loginform.value.fcm_token = ''
   
    if(this.loginform.valid ){
      this.http.post(this.baseurl+"api/seller/login", this.loginform.value).subscribe(res => {
        this.getstatus = res
       
        
        if(this.getstatus.status == true){
         localStorage.clear()
        
         
         if(this.getstatus.data.sub_id != ''){
          localStorage.setItem('main_sellerid', this.getstatus.data._id);
          localStorage.setItem('token', this.getstatus.data.token);
          localStorage.setItem('selleremail', this.loginform.value.email);
          localStorage.setItem('loginsession', '24');
          window.location.href='/dashboard/(seller:statistics)'
         }else{
          window.location.href='/subscription'
         }
         
         
         
        }else{

         
          if(this.getstatus.data.is_registered == false){
            if(this.getstatus.data.web_reg_step == 2){
              localStorage.setItem('selleremail', this.getstatus.data.email);
              setTimeout(() => {
                this.router.navigate(['otp/' + this.getstatus.data._id])
              }, 3000);
              
            }
            if(this.getstatus.data.web_reg_step == 3){
              console.log(this.getstatus.data)
               localStorage.setItem('sellerpassword', this.loginform.value.password);
               localStorage.setItem('selleremail', this.getstatus.data.email);
               localStorage.setItem('sellerfullname', this.getstatus.data.fullname);
               localStorage.setItem('sellercountrycode', this.getstatus.data.countrycode);
               localStorage.setItem('sellerphone', this.getstatus.data.phone);
               localStorage.setItem('token', this.getstatus.data.token);
               
              setTimeout(() => {
                this.router.navigate(['seller_register/' + this.getstatus.data._id])
              }, 3000);
             
            }
            
          }
        
        //  this.toastr.error(this.getstatus.errmessage, 'Error', {
        //   timeOut: 3000,
        // });
        }
       
     })
    }else{

      for (const control of Object.keys(this.loginform.controls)) {
        this.loginform.controls[control].markAsTouched();
      }
      return;
      
      // this.toastr.error("Les champs d'entrée sont requis", 'Error', {
      //   timeOut: 3000,
      // });
    }
   

  }

}
