import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {
  baseurl:any;main_id:any;main_email:any
  getuserprofile:any; getuserprofileRes:any ; sellerProfile:any
  makeapaymentrequest:any; makeapaymentrequestRes:any
  allbanks:any
  withdrawform!:FormGroup
  constructor(private seller:SellerserviceService, private http:HttpClient,
    private router:Router, private activateroute:ActivatedRoute,) {
      this.baseurl = seller.baseapiurl2
      this.main_id = localStorage.getItem('main_sellerid')
      this.main_email = localStorage.getItem('selleremail')

      this.getuserprofile = this.baseurl+"api/seller/getuserprofile/"+this.main_id
      this.makeapaymentrequest = this.baseurl+"api/seller/makeapaymentrequest"

      this.withdrawform = new FormGroup({
        bank_id: new FormControl('', Validators.required),
        note: new FormControl('', ),
        amount: new FormControl('',  Validators.required)
      })
     }

  ngOnInit(): void {
    this.http.get(this.getuserprofile).subscribe(res=>{
      this.getuserprofileRes = res
      if(this.getuserprofileRes.status){
        this.sellerProfile = this.getuserprofileRes.data
        this.allbanks = this.sellerProfile.banks
      }
    })
  }
  withdraw(){
    this.withdrawform.value.provider_id = this.main_id,
    this.withdrawform.value.provider_name = this.sellerProfile.fullname,
    this.withdrawform.value.email = this.main_email,
    console.log(this.withdrawform.value)

    if(this.withdrawform.valid){
      this.http.post( this.makeapaymentrequest, this.withdrawform.value).subscribe(res=>{
        this.makeapaymentrequestRes = res
        console.log( this.makeapaymentrequestRes)
        if(this.makeapaymentrequestRes.status){
          window.location.href="/dashboard/(seller:my_balance)"
        }
        
      })
    }else{
      for (const control of Object.keys(this.withdrawform.controls)) {
        this.withdrawform.controls[control].markAsTouched();
      }
      return;
    }
  }

}
