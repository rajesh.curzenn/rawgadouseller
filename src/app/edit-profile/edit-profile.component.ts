import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  registerform !: FormGroup
  main_id: any
  baseurl: any
  confirm: any
  profile: any
  getstatus: any; myPhoto: any; myFiles: string[] = []; formData: any;
  nrSelect = +33
  image:any
  constructor(private seller: SellerserviceService, private http: HttpClient,
    private router: Router) {
    this.main_id = localStorage.getItem('main_sellerid')
    this.baseurl = seller.baseapiurl2

    this.registerform = new FormGroup({
      fullname: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      phone: new FormControl('', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
      countrycode: new FormControl(''),
      ICorPassport: new FormControl('', Validators.required),
      shopname: new FormControl('', Validators.required),
    })
  }

  ngOnInit(): void {
    this.http.get(this.baseurl + "api/seller/getuserprofile/" + this.main_id).subscribe(res => {
      this.profile = res
      console.log(this.profile)
      this.registerform = new FormGroup({
        fullname: new FormControl(this.profile.data.fullname, Validators.required),
        email: new FormControl(this.profile.data.email, [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
        phone: new FormControl(this.profile.data.phone, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
        ICorPassport: new FormControl(this.profile.data.ICorPassport),
        shopname: new FormControl(this.profile.data.shopname),

      })
    })
  }
  onFileUpdate(event: any) {
    this.myFiles = [];
    for (var i = 0; i < event.target.files.length; i++) {
      this.myFiles.push(event.target.files[i]);
    }
    console.log(this.myFiles)
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.image = reader.result
        //console.log(reader.result );
    };
  }
  Submitprofile() {
    this.registerform.value.id = this.main_id
    this.registerform.value.photo = this.myPhoto
    // console.log(this.registerform.value)

    this.formData = new FormData();
    this.formData.append('fullname', this.registerform.value.fullname);
    this.formData.append('email', this.registerform.value.email);
    this.formData.append('phone', this.registerform.value.phone);
    this.formData.append('ICorPassport', this.registerform.value.ICorPassport);
    this.formData.append('shopname', this.registerform.value.shopname);

    this.formData.append('id', this.main_id);
    for (var i = 0; i < this.myFiles.length; i++) {
      this.formData.append("photo", this.myFiles[i]);
    }

    console.log(this.formData)

    this.http.post(this.baseurl + "api/seller/createseller", this.formData).subscribe(res => {
      console.log(res)
      this.getstatus = res
      if (this.getstatus.status == true) {
        // alert(this.getstatus.message)
        window.location.reload()

      }
    })

  }

}


